# Coding Test for Job Reference 84838

[[_TOC_]]

## Introduction

To answer the questions set out in the coding test and demonstrate my solutions I've set up this GitLab repository which allows the person assessing this to either build the Docker containers specified in the docker-compose.yml file and run the code directly or to access the code and SQL and run it in whichever environment they prefer. I'll provide information on where to find the code and how it should be run as I answer each of the 5 questions. 

I've chosen to answer questions 1-3 using Perl as my programming language and have opted to use MySQL to answer questions 4 and 5. In order to keep this as simple as possible for someone else to use I chose not to use any non-standard perl modules. 

If you wish to use the docker containers I created you will first need to clone this repo and build the containers using docker-compose

```
git clone https://gitlab.com/bethyates/coding-test-for-job-reference-84838.git
cd coding-test-for-job-reference-84838 
docker-compose up --build 
```

This should create three containers:
- __perl__, for running the code used in questions 1, 2 & 3
- __mysql_db__, hosts the mysql server with the primer_db database created using the normalised schema designed in response to question 4
- __codingtest-adminer-1__, an adminer instance that allows you to view the database schema and run the query written for question 5 by going to [localhost:8080](http://localhost:8080) in your web browser.     


## Question 1

Before starting to answer this question I made an assumption about the test data. The question stated that the data should have been provided as a CSV file, however as the file supplied was actually an xlsx file I converted the file to CSV and saved it as [Seq_edits.csv]( perl/resources/Seq_edits.csv) in the perl/resources directory. 

To read in the file data I decided to write a perl module with a function read_csv_file which opens the file, parses the data, stores it in an array, closes the file and returns the array. This package is found in [perl/scripts/FileReader.pm](perl/scripts/FileReader.pm). To test that the upload module works as expected I create a test, [perl/scripts/t/FileReader.t](perl/scripts/t/FileReader.t) and a test data file (which is a subset of the test data provided) [perl/scripts/t/data/FileReader.csv](perl/scripts/t/data/FileReader.csv). To run the test and check the module is returning an array of the type we expect you can run the following steps:

```
docker exec -it perl bash
prove
```

If you have chosen not to use the Docker containers navigate to  the perl/scripts/ directory before running the prove command.

This will run the test which uses FileReader.pm to read in the FileReader.csv file and check that the array returned contains the file data formatted as we would expect. 

## Question 2

This has partially been answered as part of question 1. However to test the Seq_edits.csv file uploads correctly you can run the perl script __questions_2and3.pl__, this will also run the algorithm written for question 3 and will write a results file to [perl/results/Question_3.txt](perl/results/Question_3.txt).

```
perl questions_2and3.pl 
```

The parsed data is stored in an array. Each array element is a hash which represents a single line from the file. 

```perl
  push @seqs, {
    'id' => $id,
    'original' => \@original,
    'edited'  => \@edited,
    'identical' => $identical
  };
```

The hash contains 4 key value pairs, as we will be comparing the original and result (or as I've called it here 'edited') sequences to look for differences I've opted to pre-process some of the file data to make the later comparison step easier. The hash contains the folowing key value pairs:

- 'id' => the ID from the data file
- 'original' => a reference to an array of the original sequence split into single bases
- 'edited' => a reference to an array of the result sequence split into single bases
- 'identical' => either 1 or undef, indicating whether or not the orignal sequence string passed an "eq" test to the result sequence.

## Question 3 

The algorithm I've implemented is in the file [perl/scripts/questions_2and3.pl](perl/scripts/questions_2and3.pl). It prints the results out to a file [perl/results/Question_3.txt](perl/results/Question_3.txt). 

To run and view the results of this code on the docker container

```
docker exec -it perl bash
perl questions_2and3.pl
cat ../results/Question_3.txt 
```

The algorithm described here is a very naive solution to this problem, as it only deals with simple mutations. A better way of implementing something like this would be to use a global alignment program such as Algorithm::NeedlemanWunsch from cpan or Needle from EMBOSS. This could be used to carry out the alignment stage and then the alignments processed and reported on in a similar way to what I've shown.


## Question 4

The SQL file for normalised schema I've designed are here [db/primer_db_2022-07-28.sql](db/primer_db_2022-07-28.sql). If you are running the docker containers you can view this schema using adminer by going to [localhost:8080](http://localhost:8080/?) and entering "test" as the username, "pass123" as the password and "primer_db" as the database name. From here you can inspect the database schema and view the individual table structures.

In terms of normalising the provided schema I've created two additional tables, one for chromosome data and one for type data and changed the primers and region table to store the chromsome_id and type_id as appropriate. 

I made the assumption that the start_coordinate and end_coordinate values in the region and primers tables represent distinct values so left these fields as they were.

## Question 5

The SQL statement is at [db/query/question5.sql](db/query/question5.sql). To run this using the docker containers go to the adminer page and select the SQL command option and paste in the box before hitting execute. I've populated my database with some dummy data so when I run the query I see output like this:

![pic](db/query/res/q5.png "Question 5 result from Adminer") 





