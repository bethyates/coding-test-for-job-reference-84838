use strict;
use warnings;
use FindBin qw/$Bin/;
use lib "$Bin/../.";
use Test::More;
use FileReader;

my $file = "$Bin/data/FileReader.csv";
my $data = FileReader::read_csv_file($file);
my $orig_3 = join "", @{$data->[2]->{'original'}};

is(scalar(@{$data}), 5, '5 sequence rows parsed');
is($data->[0]->{'id'}, 1, 'Testing ID 1 is 1');
is($orig_3, 'CAGCCTCACTT', 'Testing originalSeq for sequence ID 3 is correct');
is($data->[0]->{'identical'}, 1, 'Testing sequence 1 has been identified as having identical OriginalSeq and Result');

done_testing();