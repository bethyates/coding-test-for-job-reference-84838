#!/usr/local/bin/perl
use strict;
use warnings;
use FindBin;
use lib $FindBin::Bin; 
use FileReader;
use List::Util qw(max);


# Q2 File read in and data saved to an array of hashes
my $file = '/home/resources/Seq_edits.csv';
my @seqs = @{FileReader::read_csv_file($file)};


# Q3 Algorithm to process differences, results are printed to a text file. 

my ($match, $deletion, $insertion, $mutation) = (0,0,0,0);
my $changes = {
  'matches' => [],
  'mutations' => {},
  'insertions' => {},
  'deletions' => {} 
};

foreach my $pair (@seqs){
  my $pair_number = $pair->{'id'}; 
  my @original = @{$pair->{'original'}};
  my @edited = @{$pair->{'edited'}};
  my $count_o = scalar(@original);
  my $count_e = scalar(@edited);
  my $max = max( $count_o, $count_e);;
  my $match_str;

  if ($pair->{'identical'}){
    for (my $i = 0; $i < $max; $i++) {
      $match_str .= '*';
    }
    $match++;
    push @{$changes->{'matches'}}, $pair_number;
  } else {
    for(my $i = 0; $i < $max; $i++) {
      my $o = $original[$i];
      my $e = $edited[$i];  
      if ($o eq  $e){
        # we have a match
        $match_str .= '*';
      } else {
        if ($count_o  == $count_e){
          # we have a mutation
          $match_str .= '$'; 
        } elsif ($count_o > $count_e){
          # we have a deletion
          $match_str .= "-";
          splice @edited, $i, 0, '-';
        } elsif ($count_e > $count_o){
          # we have an insertion
          $match_str .= "+";
          splice @original, $i, 0, '-';
        }
      }
    }
  }

  $pair->{'match_string'} = $match_str;    
  $pair->{'processed_original'} = \@original;  
  $pair->{'processed_result'} = \@edited;

  $insertion++ if $match_str =~/\+/;
  $deletion++ if $match_str =~/\-/;
  $mutation++ if $match_str =~/\$/;

  if ($match_str =~/\$|\+|\-/){
    my (@subs, @del, @ins);
    my @match = split //, $match_str;
    for(my $m = 0; $m < scalar @match; $m++){
      my $type = $match[$m];
      if ($type =~/\$/){ 
        my $o_char = $pair->{'original'}->[$m];
        my $e_char = $pair->{'edited'}->[$m];
        my $pos = $m + 1;
        push @subs, "  Sequence ID $pair_number has a mutation at position $pos $o_char > $e_char.\n"; 
      } elsif ($type =~/\+/){ 
        my $pos = $m + 1;
        push @ins, "  Sequence ID $pair_number has a insertion at position $pos.\n"; 
      }  elsif ($type =~/\-/){
        my $pos = $m + 1;
        push @del, "  Sequence ID $pair_number has a deletion at position $pos.\n"; 
      } 
    } 
    
    if (scalar @subs > 0 ){ $changes->{'mutation'}->{$pair_number} = \@subs; }
    if (scalar @ins > 0 ){ $changes->{'insertion'}->{$pair_number} = \@ins; }
    if (scalar @del > 0 ){ $changes->{'deletion'}->{$pair_number} = \@del; }
  } 
}

my $resfile = '/home/results/Question_3.txt';
open(my $out, '>', $resfile) or die "Could not open '$resfile' $!";


print $out "$match of the result strands are an exact match to the original sequence:\n";
my $match_ids =  join ', ' , @{$changes->{'matches'}};
print $out "  Pairs with sequence IDs $match_ids.\n\n";

print $out "$mutation of the result strands contain one or more mutations.\n";
foreach my $seq_id (sort keys %{$changes->{'mutation'}} ){
  foreach my $sub (@{$changes->{'mutation'}->{$seq_id}}) {
    print $out $sub;
  }
}

print $out "\n$insertion of the result strands contain an insertion.\n";
my $ins_ids =  join ', ' , sort keys %{$changes->{'insertion'}};
foreach my $seq_id (sort keys %{$changes->{'insertion'}} ){
  foreach my $ins (@{$changes->{'insertion'}->{$seq_id}}) {
    my $orig = join '', @{$seqs[$seq_id -1]->{'processed_original'}};
    my $edit = join '', @{$seqs[$seq_id -1]->{'processed_result'}};
    print $out $ins;
    print $out "\toriginal: $orig\n";
    print $out "\t  edited: $edit\n\n"
  }
}

print $out "$deletion of the result strands contain a deletion.\n";
my $del_ids =  join ', ' , sort keys %{$changes->{'deletion'}};
foreach my $seq_id (sort keys %{$changes->{'deletion'}} ){
  foreach my $ins (@{$changes->{'deletion'}->{$seq_id}}) {
    my $orig = join '', @{$seqs[$seq_id -1]->{'processed_original'}};
    my $edit = join '', @{$seqs[$seq_id -1]->{'processed_result'}};
    print $out $ins;
    print $out "\toriginal: $orig\n";
    print $out "\t  edited: $edit\n\n"
  }
}

close ($out);