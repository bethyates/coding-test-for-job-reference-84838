package FileReader;
use strict;
use warnings;

sub read_csv_file {
  my ($file) = @_;
  open(my $csv, '<', $file) or die "Could not open '$file' $!";

  my @seqs;

  while (my $line = <$csv>){
    chomp $line;
    next if $line !~/^\d/; # ignore file header line if present
    my ($id, $orig, $res) = split(/,/, $line);

    # Could add in some logic here to test that the lines match what we are expecting
    # e.g. check $id is a number, $orig and $res match strings of ACTG 

    my $identical = $orig eq $res ? 1 : 0;
    my @original = split//, $orig;
    my @edited = split//, $res;

    push @seqs, {
      'id' => $id,
      'original' => \@original,
      'edited'  => \@edited,
      'identical' => $identical
    };
  }

  close($csv) || die "Couldn't close file $file properly";
  return \@seqs;
}

1;


