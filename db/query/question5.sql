SELECT lp.id as primer_id, lp.score as score, lp.dna_sequence as sequence
FROM primer_pair AS pp
LEFT JOIN region AS r ON pp.region_id = r.id
LEFT JOIN primers AS lp ON pp.left_primer_id = lp.id
WHERE r.id = 4
ORDER BY lp.score DESC;