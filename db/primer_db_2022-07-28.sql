# ************************************************************
# Sequel Ace SQL dump
# Version 20033
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: localhost (MySQL 5.7.39)
# Database: primer_db
# Generation Time: 2022-07-28 14:14:58 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table chromosome
# ------------------------------------------------------------

DROP TABLE IF EXISTS `chromosome`;

CREATE TABLE `chromosome` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `chromosome` WRITE;
/*!40000 ALTER TABLE `chromosome` DISABLE KEYS */;

INSERT INTO `chromosome` (`id`, `name`)
VALUES
	(1,'1'),
	(2,'3'),
	(3,'2'),
	(4,'4'),
	(5,'X');

/*!40000 ALTER TABLE `chromosome` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table primer_pair
# ------------------------------------------------------------

DROP TABLE IF EXISTS `primer_pair`;

CREATE TABLE `primer_pair` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `region_id` int(11) unsigned NOT NULL,
  `left_primer_id` int(11) unsigned NOT NULL,
  `right_primer_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fkregion` (`region_id`),
  KEY `fkleft` (`left_primer_id`),
  KEY `fkright` (`right_primer_id`),
  CONSTRAINT `fkleft` FOREIGN KEY (`left_primer_id`) REFERENCES `primers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fkregion` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`),
  CONSTRAINT `fkright` FOREIGN KEY (`right_primer_id`) REFERENCES `primers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `primer_pair` WRITE;
/*!40000 ALTER TABLE `primer_pair` DISABLE KEYS */;

INSERT INTO `primer_pair` (`id`, `region_id`, `left_primer_id`, `right_primer_id`)
VALUES
	(1,4,1,2),
	(2,4,3,2),
	(3,4,4,2),
	(4,5,5,6);

/*!40000 ALTER TABLE `primer_pair` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table primers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `primers`;

CREATE TABLE `primers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) unsigned NOT NULL,
  `score` int(11) NOT NULL,
  `start_coordinate` int(11) NOT NULL,
  `end_coordinate` int(11) NOT NULL,
  `chromosome_id` int(10) unsigned NOT NULL,
  `dna_sequence` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fkPrimerType` (`type_id`),
  KEY `fkPrimerChr` (`chromosome_id`),
  CONSTRAINT `fkPrimerChr` FOREIGN KEY (`chromosome_id`) REFERENCES `chromosome` (`id`),
  CONSTRAINT `fkPrimerType` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `primers` WRITE;
/*!40000 ALTER TABLE `primers` DISABLE KEYS */;

INSERT INTO `primers` (`id`, `type_id`, `score`, `start_coordinate`, `end_coordinate`, `chromosome_id`, `dna_sequence`)
VALUES
	(1,2,100,22,47,2,'ACTGTGTGTTAAACACATCCGGGATT'),
	(2,1,80,123,143,2,'TGTGTATAAGGCATCAGTGCT'),
	(3,2,65,12,28,2,'TGTGTATAAGGCATCAGTGCT'),
	(4,2,27,36,51,2,'ACCGTAACACGTAGCC'),
	(5,1,87,243,268,5,'CCCTGTGTGTTCGACACATCCGGGAT'),
	(6,1,64,321,346,5,'ACTGTGTGTTAAACACATCCGGGATT'),
	(7,1,87,243,268,5,'CCCTGTGTGTTCGACACATCCGGGAT'),
	(8,2,48,73,89,3,'ACCGTAACACGTAGCC'),
	(9,1,64,321,346,3,'ACTGTGTGTTAAACACATCCGGGATT');

/*!40000 ALTER TABLE `primers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table region
# ------------------------------------------------------------

DROP TABLE IF EXISTS `region`;

CREATE TABLE `region` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `chromosome_id` int(2) unsigned NOT NULL,
  `start_coordinate` int(11) NOT NULL,
  `end_coordinate` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fkRegionChr` (`chromosome_id`),
  CONSTRAINT `fkRegionChr` FOREIGN KEY (`chromosome_id`) REFERENCES `chromosome` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;

INSERT INTO `region` (`id`, `chromosome_id`, `start_coordinate`, `end_coordinate`)
VALUES
	(1,1,80,200),
	(2,1,95,187),
	(3,2,52,898),
	(4,2,1,156),
	(5,2,400,460),
	(6,5,1,600),
	(7,4,40,380),
	(8,3,30,436);

/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `type`;

CREATE TABLE `type` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;

INSERT INTO `type` (`id`, `type`)
VALUES
	(1,'Right'),
	(2,'Left');

/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
